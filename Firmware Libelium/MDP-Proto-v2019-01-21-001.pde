/*  
 * Implementation:    Pablo Varela - Kinetix
   
   Oportunidades de mejora:

   - Probar el Active Data Rate con verificación que ante un error ocurren los reintentos de envio
   - Probar y hacer funcionar el envío con confirmación
   - Probar el modo OTA
   - Repasar tema que el frame incial tiene menos largo, esto debe estar asociado a como se crea la instancia de la clase frame 
   - Poner a dormir el dispositivo y despertarlo cada x tiempo, esto en conjunción con la alimentación del panel solar
   - Logueo a la SD Card como metodo de tener un backup de la información.
   - Reboot cada n loops, esto es por recomendación de Libelium
   - Parametrización del delay, no funcionó hay que ver si 
   - 
    
 */

#include <WaspSensorAgr_v30.h>
#include <WaspLoRaWAN.h>
#include <WaspFrame.h>
#include <WaspSD.h>


// Device parameters for Back-End registration
////////////////////////////////////////////////////////////
//char DEVICE_EUI[]  = "0102030405060708";
  char DEVICE_EUI[]  = "0004A30B00235542";//Esto es solo para OTA
//char DEVICE_ADDR[] = "05060708";
  char DEVICE_ADDR[] = "d1412b7f";
char NWK_SESSION_KEY[] = "7d5242ef9e3c63d95daa934f4b894740";
char APP_SESSION_KEY[] = "d82fc7322c7238a128fda4935b9870c6";
char APP_KEY[] = "d82fc7322c7238a128fda4935b9870c6";
char moteID[]="nkntx";
////////////////////////////////////////////////////////////



//Variable to store the read value

float value_soil_temp;// Variable de temperatura de suelo
//float watermark2,watermark3; //Variable de humedad de suelo (soil moisture)
int watermark2,watermark3;
float par_value,par_radiation; //Variables para la lectura de luz par
float tempAmb,hum,pres;// Variables ambientales que  se leen del BME 280 conectado en el puerto F

//Variable to handle LoRAWAN

uint8_t socket = SOCKET0;
uint8_t error;
char datasend[]="0102030405060708090A0B0C0D0E0F";
//char datasend[]="";
//uint8_t datasend;
uint8_t PORT = 1;
uint8_t present;
//uint16_t uplink_count;
long uplink_count;
//Instance object

pt1000Class pt1000Sensor; // Soil Temp
watermarkClass wmSensor2(SOCKET_2);//Soil Moisture E 
watermarkClass wmSensor3(SOCKET_3);//Soil Moisture C
radiationClass radSensor;//PAR Sensor B

//Functiones realizadas para ser llamadas tanto en setup() como en loop()

void ReadPrintSensors(bool imp)
{  

  /*Funcion para tomar lecturas de todos los sensores, el paramentro de entrada 
  es una variable booleana con la que se controla si los valores se imprimen en
  el puerto serial USB
  */
  
  // Part 1: Read the PT1000 sensor 
  value_soil_temp = pt1000Sensor.readPT1000();  

  // Part 2: Read the Watermarks sensors one by one 
    
  watermark2 = (int)wmSensor2.readWatermark();      
  watermark3 = (int)wmSensor3.readWatermark();

  // Part 3: Read PAR sensor      

  par_value = radSensor.readRadiation();
  par_radiation = par_value/0.0002;

  // Part 4: Read Temp,hum,pres

  tempAmb = Agriculture.getTemperature();
  hum = Agriculture.getHumidity();
  pres = Agriculture.getPressure();
      
  // Part 5: USB printing

  if (imp)
  {
    USB.ON();  
    // Print the PT1000 temperature value through the USB - PORT D
    USB.print(F("PT1000: "));
    USB.printFloat(value_soil_temp,3);
    USB.println(F(" C"));  
    // Print the watermark measures PORT E & C
    USB.print(F("Watermark E - Frequency: "));
    USB.print(watermark2);
    USB.println(F(" Hz"));  
    USB.print(F("Watermark C - Frequency: "));
    USB.print(watermark3);
    USB.println(F(" Hz"));  
    // Print PAR read sensor - PORT B
    USB.print(F("Radiation: "));
    USB.print(par_radiation);
    USB.println(F("umol·m-2·s-1"));
    // Print Temperature 
    USB.print(F("Temperatura Ambiente: "));
    USB.print(tempAmb);
    USB.println(F(" C"));
    // Print Humidity
    USB.print(F("Humedad"));
    USB.print(hum);
    USB.println(" %");
    // Print Pressure
    USB.print(F("Presion"));
    USB.print(pres);
    USB.println(F(" Pa"));
    USB.OFF();
  
  }
   
}

void LoRAWANSetUp(bool ADR,uint8_t DR)
{
  
  /*
  Función donde se realiza el setup de la comunicación LoRAWAN los pasos son:
  1. switch on
  2. Reset to factory default values
  3. Set/Get Device EUI
  4. Set/Get Device Address
  5. Set Network Session Key
  6. Set Application Session Key
  7. Set retransmissions for uplink confirmed packet
  8. Set application key
  9. For 900MHz US bands with gateways limited to 8 channels, disable the unavailable channels
  10.Set Adaptive Data Rate
  11.Set Automatic Reply
  12.Save configuration
  */
  
  USB.ON();
  USB.println(F("LoRaWAN example - Module configuration"));
 
  //////////////////////////////////////////////
  // 1. switch on
  //////////////////////////////////////////////

  error = LoRaWAN.ON(socket);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("1. Switch ON OK"));     
  }
  else 
  {
    USB.print(F("1. Switch ON error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 2. Reset to factory default values
  //////////////////////////////////////////////

  error = LoRaWAN.factoryReset();

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("2. Reset to factory default values OK"));     
  }
  else 
  {
    USB.print(F("2. Reset to factory error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 3. Set/Get Device EUI
  //////////////////////////////////////////////

  // Set Device EUI
  error = LoRaWAN.setDeviceEUI(DEVICE_EUI);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("3.1. Set Device EUI OK"));     
  }
  else 
  {
    USB.print(F("3.1. Set Device EUI error = ")); 
    USB.println(error, DEC);
  }

  // Get Device EUI
  error = LoRaWAN.getDeviceEUI();

  // Check status
  if( error == 0 ) 
  {
    USB.print(F("3.2. Get Device EUI OK. ")); 
    USB.print(F("Device EUI: "));
    USB.println(LoRaWAN._devEUI);
  }
  else 
  {
    USB.print(F("3.2. Get Device EUI error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 4. Set/Get Device Address
  //////////////////////////////////////////////

  // Set Device Address
  error = LoRaWAN.setDeviceAddr(DEVICE_ADDR);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("4.1. Set Device address OK"));     
  }
  else 
  {
    USB.print(F("4.1. Set Device address error = ")); 
    USB.println(error, DEC);
  }
  
  // Get Device Address
  error = LoRaWAN.getDeviceAddr();

  // Check status
  if( error == 0 ) 
  {
    USB.print(F("4.2. Get Device address OK. ")); 
    USB.print(F("Device address: "));
    USB.println(LoRaWAN._devAddr);
  }
  else 
  {
    USB.print(F("4.2. Get Device address error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 5. Set Network Session Key
  //////////////////////////////////////////////
 
  error = LoRaWAN.setNwkSessionKey(NWK_SESSION_KEY);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("5. Set Network Session Key OK"));     
  }
  else 
  {
    USB.print(F("5. Set Network Session Key error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 6. Set Application Session Key
  //////////////////////////////////////////////

  error = LoRaWAN.setAppSessionKey(APP_SESSION_KEY);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("6. Set Application Session Key OK"));     
  }
  else 
  {
    USB.print(F("6. Set Application Session Key error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 7. Set retransmissions for uplink confirmed packet
  //////////////////////////////////////////////

  // set retries
  error = LoRaWAN.setRetries(7);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("7.1. Set Retransmissions for uplink confirmed packet OK"));     
  }
  else 
  {
    USB.print(F("7.1. Set Retransmissions for uplink confirmed packet error = ")); 
    USB.println(error, DEC);
  }
  
  // Get retries
  error = LoRaWAN.getRetries();

  // Check status
  if( error == 0 ) 
  {
    USB.print(F("7.2. Get Retransmissions for uplink confirmed packet OK. ")); 
    USB.print(F("TX retries: "));
    USB.println(LoRaWAN._retries, DEC);
  }
  else 
  {
    USB.print(F("7.2. Get Retransmissions for uplink confirmed packet error = ")); 
    USB.println(error, DEC);
  }


  //////////////////////////////////////////////
  // 8. Set application key
  //////////////////////////////////////////////

  error = LoRaWAN.setAppKey(APP_KEY);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("8. Application key set OK"));     
  }
  else 
  {
    USB.print(F("8. Application key set error = ")); 
    USB.println(error, DEC);
  }

  //////////////////////////////////////////////
  // 9. For 900MHz US bands with gateways limited 
  // to 8 channels, disable the unavailable channels
  //////////////////////////////////////////////

  for (int ch = 8; ch <= 71; ch++)
  {
    error = LoRaWAN.setChannelStatus(ch, "off");

    // Check status
    if( error == 0 )
    {
      
      USB.print(F("9. Channel "));
      USB.print(ch);
      USB.println(F(" status set off"));
      
    
    }
    else
    {
            
      USB.print(F("9. Channel "));
      USB.print(ch);
      USB.print(F(" status set error = "));
      USB.println(error, DEC);
    }
  }


  //////////////////////////////////////////////
  // 10. Set Adaptive Data Rate
  //////////////////////////////////////////////

  // set ADR
  
  if (ADR)
  {
    error = LoRaWAN.setADR("on");

    // Check status
    if( error == 0 ) 
    {
      USB.println(F("10.1. Set Adaptive data rate status to on OK"));     
    }
    else 
    {
      USB.print(F("10.1. Set Adaptive data rate status to on error = ")); 
      USB.println(error, DEC);
    }
  
    // Get ADR
    error = LoRaWAN.getADR();

    // Check status
    if( error == 0 ) 
    {
      USB.print(F("10.2. Get Adaptive data rate status OK. ")); 
      USB.print(F("Adaptive data rate status: "));
      if (LoRaWAN._adr == true)
      {
        USB.println("on");      
      }
      else
      {
        USB.println("off");
      }
    }
    else 
    {
      USB.print(F("10.2. Get Adaptive data rate status error = ")); 
      USB.println(error, DEC);
    }
  }
  else
  {
    if ( DR < 5)
    {
    
      error = LoRaWAN.setDataRate(DR);
    
      // Check status
      if( error == 0 ) 
      {
        USB.println(F("10. Data rate set OK"));     
      }
      else 
      {
        USB.print(F("10. Data rate set error = ")); 
        USB.println(error, DEC);
      }
     }
     else 
     {
       USB.println(F("10. Wrong data rate set to default 2"));
       
       error = LoRaWAN.setDataRate(2);
      
        if( error == 0 ) 
        {
          USB.println(F("10. Data rate set OK"));     
        }
        else 
        {
          USB.print(F("10. Data rate set error = ")); 
          USB.println(error, DEC);
        }
      } 
   }
  
  //////////////////////////////////////////////
  // 11. Set Automatic Reply
  //////////////////////////////////////////////

  // set AR
  error = LoRaWAN.setAR("on");
  //error = LoRaWAN.setAR("off");
  // Check status
  if( error == 0 ) 
  {
    USB.println(F("11.1. Set automatic reply status to on OK"));     
  }
  else 
  {
    USB.print(F("11.1. Set automatic reply status to on error = ")); 
    USB.println(error, DEC);
  }
  
  // Get AR
  error = LoRaWAN.getAR();

  // Check status
  if( error == 0 ) 
  {
    USB.print(F("11.2. Get automatic reply status OK. ")); 
    USB.print(F("Automatic reply status: "));
    if (LoRaWAN._ar == true)
    {
      USB.println("on");      
    }
    else
    {
      USB.println("off");
    }
  }
  else 
  {
    USB.print(F("11.2. Get automatic reply status error = ")); 
    USB.println(error, DEC);
  }

  //////////////////////////////////////////////
  // 12. Save configuration
  //////////////////////////////////////////////
  
  error = LoRaWAN.saveConfig();

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("12. Save configuration OK"));     
  }
  else 
  {
    USB.print(F("12. Save configuration error = ")); 
    USB.println(error, DEC);
  }
  //LoRaWAN.OFF(socket);
  USB.OFF();
}

void SendDataLWABP(char data[],bool confirmed,int upLkCnt)
{
  /*
  Función para ordenar el envío de la información en LoRaWAN. 
  Los paramentros de entrada son el payload, una varialble booleana
  para determinar si se pide confirmación y el uplinkcounter.

  1. Switch on
  2. Join Network
  3. Send Confirmed or Unconfirmed packet
  4. Set up uplink counter
  5. Switch off
  
  */ 
  
  //////////////////////////////////////////////
  // 1. Switch on
  //////////////////////////////////////////////
  
  USB.ON();
  
  error = LoRaWAN.ON(socket);
  
  // Check status
  if( error == 0 ) 
  {
    USB.println(F("1. Switch ON OK"));     
  }
  else 
  {
    USB.print(F("1. Switch ON error = ")); 
    USB.println(error, DEC);
  }
  
  
  //////////////////////////////////////////////
  // 2. Join network
  //////////////////////////////////////////////

  error = LoRaWAN.joinABP();

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("2. Join network OK"));     
  }
  else 
  {
    USB.print(F("2. Join network error = ")); 
    USB.println(error, DEC);
  }
    
  //////////////////////////////////////////////
  // 3. Send Confirmed or Unconfirmed packet 
  //////////////////////////////////////////////
  if (confirmed)
  {  
    //error = LoRaWAN.sendConfirmed( PORT, data, payloadlenght);
    error = LoRaWAN.sendConfirmed( PORT, data);
  }
  else 
  {
    //error = LoRaWAN.sendUnconfirmed( PORT, data, payloadlenght);
    error = LoRaWAN.sendUnconfirmed( PORT, data);  
  }
   // Error messages:
    /*
     * '6' : Module hasn't joined a network
     * '5' : Sending error
     * '4' : Error with data length    
     * '2' : Module didn't response
     * '1' : Module communication error   
    */
  // Check status
  if( error == 0 ) 
  {
    if (confirmed)
    {
      USB.print(F("3. Send Confirmed packet Ok"));
      USB.println();
    }
    else 
    {
      USB.print(F("3. Send Unconfirmed packet Ok"));
      USB.println();
    }
    
    if (LoRaWAN._dataReceived == true)
    { 
      USB.print(F("   There's data on port number "));
      USB.print(LoRaWAN._port,DEC);
      USB.print(F(".\r\n   Data: "));
      USB.println(LoRaWAN._data);
    }
  }
  else 
  {
    USB.print(F("3. Send Confirmed packet error = ")); 
    USB.println(error, DEC);
  }

  //////////////////////////////////////////////
  // 4. Set up uplink counter
  //////////////////////////////////////////////
  
  LoRaWAN._upCounter=upLkCnt;
  
  if( error == 0 ) 
  {
    USB.print(F("4. Set up UpLinkCounter Ok --"));     
    USB.print(F("Se envio el paquete numero: "));
    USB.print(upLkCnt);
    USB.print(F(" con el UpLinkCounter en "));
    USB.println(LoRaWAN._upCounter);
  }
  else 
  {
    USB.print(F("4.Set up UpLinkCounter error" )); 
    USB.println(error, DEC);
  }
    
  //////////////////////////////////////////////
  // 5. Switch off
  //////////////////////////////////////////////
  
    
  
  // Apago el módulo de comunicación
    
  error = LoRaWAN.OFF(socket);

  // Check status
  if( error == 0 ) 
  {
    USB.println(F("5. Switch OFF OK"));     
    USB.println();
  }
  else 
  {
    USB.print(F("5. Switch OFF error = ")); 
    USB.println(error, DEC);
  }
    
   USB.OFF();
}
bool string_to_hexstring(uint8_t *string, char *hexstring, uint16_t hexstring_length)
//bool string_to_hexstring(uint8_t *string, uint8_t hexstring, uint16_t hexstring_length)
{
 /*
 Función integrada para convertir string ASCII a Hexadecimal. 
 Inpunt: string pointer
 Output: hextring pointer 
 */
    
    uint16_t hexstring_index = 0;
    while(string[hexstring_index/2] != 0 && hexstring_index < hexstring_length-2)
    {
        char c = string[hexstring_index/2];
        byte nib1 = (c >> 4) & 0x0F;
        byte nib2 = (c >> 0) & 0x0F;
        hexstring[hexstring_index++] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        hexstring[hexstring_index++] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    hexstring[hexstring_index++] = 0;
    return string[hexstring_index/2] == 0;
}

// Setup Function
void setup()
{
  
  // Turn on the sensor board
  
  Agriculture.ON();
  
  USB.ON();
     
  // Setup LoRAWAN Params
  
  uplink_count=0;//contador generico para llevar el numero de loops.

  LoRaWAN.setUpCounter(0);//Llevo el contador de uplinks que lleva el módulo LoRaWAN a cero 
  
  LoRAWANSetUp(false,2); //Set up del mpdulo LoRaWAN, con Adaptive Data Rate apagada.
        
  USB.OFF(); //Apago la interfaz serial

}

// Main Loop

void loop()
{
  //Levanto la interfaz sería para impresión de mensajes
  
  USB.ON();
      
  //Lectura de sensores
  
  ReadPrintSensors(false);

  /*Creo instancia de la clase frame para armado de payload. 
  Los valores que se agregan son como un "append" en el paylod.
  */

  frame.createFrame(ASCII); 
  frame.addSensor(SENSOR_AGR_SOILTC,value_soil_temp);
  frame.addSensor(SENSOR_AGR_SOIL_C,watermark3);
  frame.addSensor(SENSOR_AGR_SOIL_E,watermark2);
  frame.addSensor(SENSOR_AGR_PAR,par_radiation);
  frame.addSensor(SENSOR_AGR_TC,tempAmb);
  frame.addSensor(SENSOR_AGR_HUM,hum);
  frame.addSensor(SENSOR_AGR_PRES,pres);
    
  uint16_t largo = frame.length;//Guarda el largo del frame

  //Convertion to HEX, paso el largo por 2 dado que 1 byte pasa a ocupar 2 indices del array en Hexa.
      
  if (!(string_to_hexstring(frame.buffer,datasend,largo*2)))
  {
   //USB.println(F("me quede aca")); 
  }

  //LoRaWAN.setUpCounter(uplink_count);

  //Send Data through LoRAWAN Netwok ABP Method

  SendDataLWABP(datasend,false,uplink_count); // Ejectuo envío pasando array,confirmación y uplink count.

  uplink_count++;
      
  USB.OFF();

  delay(1800000);
  //delay(10000);
}
