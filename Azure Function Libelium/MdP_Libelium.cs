
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices.Client;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Net.Http;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

namespace MdP_Libelium
{
    public static class MdP_Libelium
    {

        // Connection String del Device para autentiar contra el Azure IoT Hub
        //private readonly static string s_connectionString = "HostName=mdp-iothub.azure-devices.net;DeviceId=TeliotA1;SharedAccessKey=kwb2MA1pStesCTTzFGmxWpm/F8TgtxkE062XtGJNj/k=";
        private readonly static string s_connectionString_lib_A1 = "HostName=mdp-iothub.azure-devices.net;DeviceId=Libelium-A1;SharedAccessKey=6ZlLPNrHWXpNiQ8/XMhzO9PR9DFsxGS2K5I922OPIas=";
        private readonly static string s_connectionString_lib_HS1 = "HostName=mdp-iothub.azure-devices.net;DeviceId=Libelium-HS1;SharedAccessKey=6zIMjCHTVNwnUsdZWZmhiCYO74Tc9opbDrKhdblzbqI=";
        private readonly static string s_connectionString_lib_HS2 = "HostName=mdp-iothub.azure-devices.net;DeviceId=Libelium-HS2;SharedAccessKey=G7LhwJuxCGvqw5fjpfsZv4EY3pjCdtwCcDoQEEpRdkI=";
        private readonly static string s_connectionString_lib_TS1 = "HostName=mdp-iothub.azure-devices.net;DeviceId=Libelium-TS1;SharedAccessKey=FLPHhxZYUYImIf7zBHHJQoH44TntCjRtu5LZtWtzSo8=";


        // Parametros para conexión a la base CosmosDB
        private static DocumentClient cosmos_client = null;
        private readonly static string cosmos_url = "https://mdp-viveroiot.documents.azure.com:443";
        private readonly static string cosmos_database = "dbviverioiot";
        private readonly static string cosmos_key = "iexYaARCaE6l7fBXPAWHCiZgX2h6IssYVnlX5SpkQNISCIQpc12ZW4zGczrVEJsCQrXI3tkTo60Zfm1CjtyZJg==";


        [FunctionName("MdP_Libelium")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {

            try
            {

                // Parsing body del payload
                LoRaFrame data = req.Content.ReadAsAsync<LoRaFrame>().GetAwaiter().GetResult();

                if (data != null)
                {
                    string payload_str = Encoding.UTF8.GetString(Convert.FromBase64String(data.Params.Payload));
                    Console.WriteLine("{0} > Mensaje recibido - {1}", DateTime.Now, payload_str);


                    var payload_parts = payload_str.Split('#');
                    for (int i = 0; i < payload_parts.Count(); i++)
                        Console.WriteLine("{0} > Parte {1} = {2}", DateTime.Now, i, payload_parts[i]);

                    if (payload_parts.Count() == 10)
                    {
                        Dictionary<string, string> keyValuePairs = payload_parts.Skip(4).Select(value => value.Split(':')).ToDictionary(pair => pair[0], pair => pair[1]);

                        List<Exception> errores = new List<Exception>();

                        try
                        {
                            insert_Libelium_A1(keyValuePairs["PAR"], keyValuePairs["HUM"], keyValuePairs["TC"]);
                        }
                        catch (Exception e) { errores.Add(e); }
                        try
                        {
                            insert_Libelium_HS1(keyValuePairs["SOIL_C"]);
                        }
                        catch (Exception e) { errores.Add(e); }
                        try
                        {
                            insert_Libelium_HS2(keyValuePairs["SOIL_E"]);
                        }
                        catch (Exception e) { errores.Add(e); }
                        try
                        {
                            insert_Libelium_TS1(keyValuePairs["SOILTC"]);
                        }
                        catch (Exception e) { errores.Add(e); }

                        if (errores.Count == 0)
                            return new JsonResult("Ok");
                        else
                        {
                            StringBuilder sb = new StringBuilder();
                            foreach (Exception e in errores) { 
                                sb.AppendLine(e.ToString());
                                sb.AppendLine("#############");
                            }

                            throw new Exception("Errores="+errores.Count + "  \n"+ sb.ToString());
                        }
                    }
                    else
                        throw new Exception("Cantidad de datos incorrecta: " + payload_parts.Count());

                }
                else
                {
                    throw new Exception("Error al recibir los datos");
                }

            }
            catch (Exception ex)
            {
                return new JsonResult("Error: " + ex.ToString());
            }


        }

        public static int insert_Libelium_A1(string str_luzPar, string str_humedadAmbiente, string str_temperaturaAmbiente)
        {
            string device_id = "Libelium-A1";

            // Obtengo la ubicación actual del sensor mediante la tabla de mapeo (Ubicacion - Sensor)
            List<SensorUbication> sensorUbication = GetSensorUbication(device_id);

            // Si la ubicación es valida, entonces guardo la telemtría
            if (sensorUbication != null && sensorUbication.Count() > 0)
            {
                // Conecto con el IoTHuB usando el connection string del device
                DeviceClient s_deviceClient = DeviceClient.CreateFromConnectionString(s_connectionString_lib_A1, TransportType.Mqtt);

                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";

                double luzPar = Convert.ToDouble(str_luzPar, provider);
                double humedadAmbiente = Convert.ToDouble(str_humedadAmbiente, provider);
                double temperaturaAmbiente = Convert.ToDouble(str_temperaturaAmbiente, provider);

                // Crear mensaje JSON para Azure IoT Hub
                var telemetryDataPoint = new
                {
                    
                    luz_par = luzPar,                    
                    temperatura = temperaturaAmbiente,
                    humedad = humedadAmbiente,
                    ubicacion = sensorUbication.First().Location,
                    ubicacionId = sensorUbication.First().LocationId
                };

                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                // Envío las medidas recibidas al Azure IoT Hub
                s_deviceClient.SendEventAsync(message).GetAwaiter().GetResult();

                return 1;
            }
            else
            {
                throw new Exception("No se encontró una ubicación valida para el sensor " + device_id);
            }

        }

        public static int insert_Libelium_HS1(string value)
        {
            string device_id = "Libelium-HS1";

            // Obtengo la ubicación actual del sensor mediante la tabla de mapeo (Ubicacion - Sensor)
            List<SensorUbication> sensorUbication = GetSensorUbication(device_id);

            // Si la ubicación es valida, entonces guardo la telemtría
            if (sensorUbication != null && sensorUbication.Count() > 0)
            {
                // Conecto con el IoTHuB usando el connection string del device
                DeviceClient s_deviceClient = DeviceClient.CreateFromConnectionString(s_connectionString_lib_HS1, TransportType.Mqtt);

                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";

                double humedad = Convert.ToDouble(value, provider);

                // Crear mensaje JSON para Azure IoT Hub
                var telemetryDataPoint = new
                {
                    humedad_suelo= humedad,
                    ubicacion = sensorUbication.First().Location,
                    ubicacionId = sensorUbication.First().LocationId
                };

                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                // Envío las medidas recibidas al Azure IoT Hub
                s_deviceClient.SendEventAsync(message).GetAwaiter().GetResult();

                return 1;
            }
            else
            {
                throw new Exception("No se encontró una ubicación valida para el sensor " + device_id);
            }
        }

        public static int insert_Libelium_HS2(string value)
        {
            string device_id = "Libelium-HS2";

            // Obtengo la ubicación actual del sensor mediante la tabla de mapeo (Ubicacion - Sensor)
            List<SensorUbication> sensorUbication = GetSensorUbication(device_id);

            // Si la ubicación es valida, entonces guardo la telemtría
            if (sensorUbication != null && sensorUbication.Count() > 0)
            {
                // Conecto con el IoTHuB usando el connection string del device
                DeviceClient s_deviceClient = DeviceClient.CreateFromConnectionString(s_connectionString_lib_HS2, TransportType.Mqtt);

                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";

                double humedad = Convert.ToDouble(value, provider);

                // Crear mensaje JSON para Azure IoT Hub
                var telemetryDataPoint = new
                {
                    humedad_suelo = humedad,
                    ubicacion = sensorUbication.First().Location,
                    ubicacionId = sensorUbication.First().LocationId
                };

                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                // Envío las medidas recibidas al Azure IoT Hub
                s_deviceClient.SendEventAsync(message).GetAwaiter().GetResult();

                return 1;
            }
            else
            {
                throw new Exception("No se encontró una ubicación valida para el sensor " + device_id);
            }
        }

        public static int insert_Libelium_TS1(string value)
        {
            string device_id = "Libelium-TS1";

            // Obtengo la ubicación actual del sensor mediante la tabla de mapeo (Ubicacion - Sensor)
            List<SensorUbication> sensorUbication = GetSensorUbication(device_id);

            // Si la ubicación es valida, entonces guardo la telemtría
            if (sensorUbication != null && sensorUbication.Count() > 0)
            {
                // Conecto con el IoTHuB usando el connection string del device
                DeviceClient s_deviceClient = DeviceClient.CreateFromConnectionString(s_connectionString_lib_TS1, TransportType.Mqtt);

                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";

                double temperatura = Convert.ToDouble(value, provider);

                // Crear mensaje JSON para Azure IoT Hub
                var telemetryDataPoint = new
                {
                    temperatura_suelo = temperatura,
                    ubicacion = sensorUbication.First().Location,
                    ubicacionId = sensorUbication.First().LocationId
                };

                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                // Envío las medidas recibidas al Azure IoT Hub
                s_deviceClient.SendEventAsync(message).GetAwaiter().GetResult();

                return 1;
            }
            else
            {
                throw new Exception("No se encontró una ubicación valida para el sensor " + device_id);
            }
        }


        /* --- F U N C I O N E S -- A U X I L I A R E S --- */

        public static List<SensorUbication> GetSensorUbication(string deviceId)
        {
            if (cosmos_client == null)
            {
                cosmos_client = new DocumentClient(new Uri(cosmos_url), cosmos_key);
            }

            var documentCollectionUri = UriFactory.CreateDocumentCollectionUri(cosmos_database, "ubicaciones");

            // build the query
            var feedOptions = new FeedOptions() { MaxItemCount = -1 };
            var query = cosmos_client.CreateDocumentQuery<SensorUbication>(documentCollectionUri, "SELECT * FROM c WHERE c.sensorId = \"" + deviceId + "\"", feedOptions);
            var queryAll = query.AsDocumentQuery();

            // combine the results
            var results = new List<SensorUbication>();
            while (queryAll.HasMoreResults)
            {
                results.AddRange(queryAll.ExecuteNextAsync<SensorUbication>().GetAwaiter().GetResult());
            }

            return results;
        }


        /* --- E N T I D A D E S -- A U X I L I A R E S -- E V E R Y N E T -- L O R A --- */

        public partial class SensorUbication
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("sensorId")]
            public string DeviceId { get; set; }

            [JsonProperty("ubicacionId")]
            public int LocationId { get; set; }

            [JsonProperty("ubicacion")]
            public string Location { get; set; }
        }

        public partial class LoRaFrame
        {
            [JsonProperty("meta")]
            public Meta Meta { get; set; }

            [JsonProperty("params")]
            public Params Params { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }

        public partial class Meta
        {
            [JsonProperty("network")]
            public string Network { get; set; }

            [JsonProperty("packet_hash")]
            public string PacketHash { get; set; }

            [JsonProperty("application")]
            public string Application { get; set; }

            [JsonProperty("device_addr")]
            public string DeviceAddr { get; set; }

            [JsonProperty("time")]
            public double Time { get; set; }

            [JsonProperty("device")]
            public string Device { get; set; }

            [JsonProperty("packet_id")]
            public string PacketId { get; set; }

            [JsonProperty("gateway")]
            public string Gateway { get; set; }
        }

        public partial class Params
        {
            [JsonProperty("rx_time")]
            public double RxTime { get; set; }

            [JsonProperty("port")]
            public long Port { get; set; }

            [JsonProperty("counter_up")]
            public long CounterUp { get; set; }

            [JsonProperty("payload")]
            public string Payload { get; set; }

            [JsonProperty("encrypted_payload")]
            public string EncryptedPayload { get; set; }
        }
    }

}
